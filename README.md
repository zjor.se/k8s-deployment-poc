# K8S Deployment PoC

Demonstrates how to deploy to Kubernetes with GitLab CI/CD

## Steps

- Get gitlab service account token
`kubectl get secrets`
`kubectl describe secret gitlab-service-account-token-zgldj`

- Check liveliness
`kubectl port-forward gitlab-deployment-poc-67495c5d89-f22qz 8080:80`

## References

- [Automated deployments to Kubernetes with GitLab](https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/)